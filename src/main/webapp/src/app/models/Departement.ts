import { Filiere } from './Filiere';

export interface Departement {
    id: number;
    codeDepartement: string;
    intituleDepartement: string;
    filieres: Filiere[];
}
