import {MemoireStore} from './../models/MemoireStore';
import {Memoire} from './../models/Memoire';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MemoiresService {

  url: string;
  constructor(private http: HttpClient) { 
    this.url = environment.url;
  }

 ajouterMemoire(memoire: MemoireStore): Observable<Memoire> {
     return this.http.post<Memoire>(this.url + 'memoires', memoire);
 }

AllMemoires() :Observable<any>{
    return this.http.get<any>(this.url + 'memoires');
 }

// findByTitre(): Observable<any>{
//     return this.http.get<any>(this.URL +'memoires/searchByTitre');
// }

// findByDiplome(): Observable<any>{
//     return this.http.get<any>(this.URL +'memoires/searchByDiplome');
// }

// findByMotCles(): Observable<any>{
//     return this.http.get<any>(this.URL +'memoires/searchByMotCles');
// } 
 
// findByEncadreur(): Observable<any>{
//     return this.http.get<any>(this.URL +'memoires/searchByEncadreur');
// } 


public findById(id){
  return this.http.get<any>(this.url +'memoires/' +id);
 
}

}
