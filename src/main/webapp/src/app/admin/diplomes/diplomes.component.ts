import { Component, OnInit } from '@angular/core';
import { DiplomeService } from 'src/app/services/diplome.service';
import { Diplome } from 'src/app/models/Diplome';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-diplomes',
  templateUrl: './diplomes.component.html',
  styleUrls: ['./diplomes.component.scss']
})
export class DiplomesComponent implements OnInit {
  form: FormGroup;
  diplomes: Diplome[]=[];
  diplome: Diplome ={
    id:null,
    codeDiplome:"",
    intituleDiplome:""
  };
  constructor(private dipl: DiplomeService) { 

    this.dipl.diplomeAll().subscribe(a => {
      a.content.forEach(element => {
        this.diplomes.push(element);
      })
    });
  }

  ngOnInit(): void {

    this.form = new FormGroup({
      codeDiplome: new FormControl('', [Validators.required]),
     intituleDiplome: new FormControl('', [Validators.required]),
   });
  }
 

  ajouterDiplome(){
    this.dipl.addDiplome(this.diplome).subscribe(d=>{
      this.diplomes.push(d);
    })
  }

}
