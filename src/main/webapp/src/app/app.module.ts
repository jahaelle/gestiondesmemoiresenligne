import { BrowserModule } from '@angular/platform-browser';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FiliereService } from './services/filiere.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MemoiresService } from './services/memoires.service';
import { HomeComponent } from './home/home.component';
import { MemoireComponent } from './memoire/memoire.component';
import { PageComponent } from './page/page.component';
import {ButtonModule} from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import {CardModule} from 'primeng/card';
import {SidebarModule} from 'primeng/sidebar';
import {FieldsetModule} from 'primeng/fieldset';
import {PanelModule} from 'primeng/panel';
import {PanelMenuModule} from 'primeng/panelmenu';
import { FooterComponent } from './footer/footer.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { FileStorageService } from './services/file-storage.service';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
import { FilterPipe } from './filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MemoireComponent,
    PageComponent,
    FooterComponent,
    FilterPipe,
  
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    CardModule,
    SidebarModule,
    ButtonModule,
    DropdownModule,
    FieldsetModule,
    PanelModule,
    ReactiveFormsModule,
    PanelMenuModule,
    ToastModule
   
  ],
  providers: [
    FiliereService,
    MemoiresService,
    FileStorageService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
