/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Gemoire.Gemoire.dao;

import com.Gemoire.Gemoire.entity.Filiere;
import java.util.Optional;
import java.util.List;
import com.Gemoire.Gemoire.entity.Departement;
import com.Gemoire.Gemoire.entity.Memoire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jahaelle
 */
public interface FiliereDao extends JpaRepository<Filiere, Long>{
     public Optional<Filiere> findByIdAndDepartementId(long idDepartement, long idFiliere);
     public List<Filiere> findByDepartement(Departement dep);
     
    // String idFiliere = "SELECT f.id FROM Filiere as f WHERE f.intituleFiliere = :intitule";
    
    // @Query(idFiliere)
    // public Long getIdFileire(@Param("intitule")String intitule);
//    
//    String filtreParFiliere = "SELECT * FROM memoire, filiere WHERE memoire.filiere_id=filiere.id AND filiere.intituleFiliere= :intitule";
//
//   @Query(filtreParFiliere)
//  public List<Memoire> getAllMemoireByFiliere(@Param("intitule") String intitule);
}
