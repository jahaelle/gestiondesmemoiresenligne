import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { environment } from '../../environments/environment';
import { Encadreur } from '../models/Encadraeur';

@Injectable({
  providedIn: 'root'
})

export class EncadreursService {
  url: string;
  constructor(private http: HttpClient) {
    this.url = environment.url;
  }

  allEncadreurs(): Observable<any> {
    return this.http.get<any>(this.url + 'encadreurs');
  }

  addEncadreur(encadreur: Encadreur): Observable<Encadreur> {
    return this.http.post<any>(this.url + 'encadreurs', encadreur).pipe(map(e => e.body));
  }
  
  deleteEncadreur(id: number): Observable<any> {
    return this.http.delete<any>(this.url + 'encadreurs/' + id);
  }

  updateEncadreur(encadreur: Encadreur): Observable<Encadreur>{
    return this.http.post<any>(this.url + 'encadreurs/' + encadreur.id,encadreur);
  }

}
