import { Departement } from './Departement';
import { Diplome } from './Diplome';


export class Filiere {

    id: number;
    codeFiliere: string;
    intituleFiliere: string;
    departement: Departement;
    diplome: Diplome;
}
