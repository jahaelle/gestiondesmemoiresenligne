export class Encadreur{
    id: number;
    gradeEncadreur: string;
    nomEncadreur: string;
    titreEncadreur: string;
}
