export class DepartementInput{
    constructor(public id: number,
                public codeDepartement: string,
                public intituleDepartement:string
                ){}
}