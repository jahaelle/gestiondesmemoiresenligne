import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{AuthComponent} from  './admin/auth/auth.component';
import { from } from 'rxjs';
import { AppComponent } from './app.component';
import { MemoireComponent } from './memoire/memoire.component';
import { HomeComponent } from './home/home.component';
import { PageComponent } from './page/page.component';



const routes: Routes = [
  {path: '', redirectTo: 'page', pathMatch: 'full'},
 {path: 'page', component:  PageComponent },
 {path: 'memoire', component: MemoireComponent},
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule)
  }
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
