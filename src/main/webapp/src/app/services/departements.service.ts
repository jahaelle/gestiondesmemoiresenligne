import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators'
import { environment } from 'src/environments/environment';
import { Departement } from '../models/Departement';
import { Filiere } from '../models/Filiere';
import { DepartementInput } from '../models/DepartementInput';

@Injectable({
  providedIn: 'root'
})


export class DepartementsService {
  url: string;
  constructor(private http: HttpClient) {
    this.url = environment.url;
  }

  listAll(): Observable<any> {
    return this.http.get<any>(this.url + 'departements');
  }

  addDepartement(departement: Departement): Observable<Departement> {
    return this.http.post<any>(this.url + 'departements', departement).pipe(map(e => e.body));
  }


  deleteDepartement(id: number): Observable<any> {
    return this.http.delete<any>(this.url + 'departements/' + id);
  }

  updateDepartement(departement: DepartementInput): Observable<DepartementInput> {
    return this.http.put<DepartementInput>(this.url + 'departeDepartementments/' + departement.id, departement);
  }

  addFiliereDepartement(departement: Departement, filiere: Filiere): Observable<Filiere> {
    return this.http.post<any>(this.url + 'departements/' + departement.id, filiere).pipe(map(d=>d.body));
  }

  findFilieresDepart(idDep: number): Observable<Filiere[]>{
    return this.http.get<any>(this.url + 'departements/filieres/' + idDep);
  }

  findOneDepartment(idDep: number): Observable<Departement>{
    return this.http.get<any>(this.url + 'departements/' + idDep);
  }

findById(id:number):Observable<any>{
  return this.http.get<any>(this.url +"/departements/" +id);
}


}
