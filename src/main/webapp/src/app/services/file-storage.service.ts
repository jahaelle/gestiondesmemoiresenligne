import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators'
import { environment } from '../../../src/environments/environment';
import { UploadFileResponse } from './../models/UploadFileResponse';

@Injectable({
  providedIn: 'root'
})

export class FileStorageService {
  
  url: string;

  constructor(private http: HttpClient) { 
    this.url = environment.url;
  }

  uploadFile(fileToUpload: File): Observable<UploadFileResponse>{
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post<UploadFileResponse>(this.url + 'file/uploadFile', formData);
  }

  // uploadImage(imageToUpload: File): Observable<UploadFileResponse>{
  //   const formData: FormData = new FormData();
  //   formData.append('file', imageToUpload, imageToUpload.name);
  //   return this.http.post<UploadFileResponse>(this.url + 'file/uploadFile', formData);
  // }

}
