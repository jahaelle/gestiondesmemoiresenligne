package com.Gemoire.Gemoire.entity;

import java.io.Serializable;


public class UploadFileResponse implements Serializable {
    
    private final String fileName;
    private final String fileDownloadUri;
    private final String status;

    public UploadFileResponse(String fileName, String fileDownloadUri, String status) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.status = status;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileDownloadUri() {
        return fileDownloadUri;
    }
    
    public String getStatus() {
        return status;
    }
    
}
