package com.Gemoire.Gemoire.services;

import com.Gemoire.Gemoire.config.FileStoragePropertiesConfig;
import com.Gemoire.Gemoire.exception.DataAccessException;
import com.Gemoire.Gemoire.exception.FileStorageException;
import com.Gemoire.Gemoire.exception.MyFileNotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class FileStorageService {

    private Path fileStorageLocation;

    private final String path;

    @Autowired
    public FileStorageService(FileStoragePropertiesConfig fileStorageProperties) {
        this.path = fileStorageProperties.getUploadDir();
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            if (!Files.exists(this.fileStorageLocation)) {
                Files.createDirectories(this.fileStorageLocation);
            }
        } catch (IOException ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String storeFile(InputStream fileInputStream, FormDataContentDisposition fileMetaData) throws DataAccessException, FileStorageException {
        String fileName = null;
        // Normalize file name
        fileName = StringUtils.cleanPath(fileMetaData.getFileName());
        
        if (fileName != null) {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence!");
            }
            try {

                Files.createDirectories(this.fileStorageLocation);
                // Copy file to the target location (Replacing existing file with the same name)
                Path targetLocation = this.fileStorageLocation.resolve(fileName);
                Files.copy(fileInputStream, targetLocation, StandardCopyOption.REPLACE_EXISTING);
                return fileName;
            } catch (IOException ex) {
                throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
            }
        } else {
            throw new FileStorageException("File name can't be empty...");
        }
    }

    public Resource loadFileAsResource(String fileName) throws DataAccessException, MyFileNotFoundException {
        if (fileName != null) {
            try {
                Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
                Resource resource = new UrlResource(filePath.toUri());
                if (resource.exists()) {
                    return resource;
                } else {
                    throw new MyFileNotFoundException("File not found " + fileName);
                }
            } catch (MalformedURLException ex) {
                throw new MyFileNotFoundException("Malformed URL to access at this file", ex);
            }
        } else {
            throw new MyFileNotFoundException("File name can't be empty");
        }
    }
}
