import { Component, OnInit } from '@angular/core';
import { Diplome } from 'src/app/models/Diplome';
import { Etudiant } from 'src/app/models/Etudiant';
import { Encadreur } from 'src/app/models/Encadraeur';
import { Filiere } from 'src/app/models/Filiere';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FileStorageService } from 'src/app/services/file-storage.service';
import { UploadFileResponse } from 'src/app/models/UploadFileResponse';
import { MemoiresService } from 'src/app/services/memoires.service';
import { EtudiantService } from 'src/app/services/etudiant.service';
import { DiplomeService } from 'src/app/services/diplome.service';
import { Memoire } from 'src/app/models/Memoire';
import { EncadreursService } from 'src/app/services/encadreurs.service';
import { FiliereService } from 'src/app/services/filiere.service';
import { MemoireStore } from 'src/app/models/MemoireStore';
import { Router } from '@angular/router';
import {MessageService} from 'primeng/api';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-memoires',
  templateUrl: './memoires.component.html',
  styleUrls: ['./memoires.component.scss']
})
export class MemoiresComponent implements OnInit {
  search;
  page: Number = 1; 
  totalrecords: string;
  diplomes: Diplome[];
  diplome: Diplome;
  etudiants: Etudiant[];
  etudiant: Etudiant;
  encadreurs: Encadreur[];
  encadreur: Encadreur;
  filieres: Filiere[];
  memoires: Memoire[];
  filiere: Filiere;
  showmodal: string;
  memoire2: Memoire = new Memoire();
  searchText;
  
  constructor(private fileService: FileStorageService,
    private memService: MemoiresService,
    private etudService: EtudiantService,
    private diplomeService: DiplomeService,
    private encadService: EncadreursService,
    private filireService: FiliereService,
    private messageService: MessageService,
    private router: Router,
    protected _sanitizer: DomSanitizer) { 

      this.getAllMemoire();
    }

  form: FormGroup;
  fileToUpload: File = null;
  imageToUpload: File = null;
  uploadResponse: UploadFileResponse;

  ngOnInit(): void {
    this.getAllMemoire();
    this.getAllDiplome();
    this.getAllEncad();
    this.getAllEtud();
    this.getAllFiliere();
    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      diplome: new FormControl('', [Validators.required]),
      session: new FormControl('', [Validators.required]),
      resume: new FormControl('', [Validators.required]),
      abstract: new FormControl('', [Validators.required]),
      contenu: new FormControl('', [Validators.required]),
      // image: new FormControl(''),
      etudiant: new FormControl('', [Validators.required]),
      encadreur: new FormControl('', [Validators.required]),
      filiere: new FormControl('', [Validators.required])
     
    });
  }

  addMemoire(){
    let memoire: MemoireStore = {
      titre:  <string> this.form.get("title").value,
      contenu: "",
      resume: <string> this.form.get("resume").value,
      abstract_memoire: <string> this.form.get("abstract").value,
      // image: "",
      session: <string> this.form.get("session").value,
      filiere: this.filiere,
      etudiant: this.etudiant,
      encadreur: this.encadreur,
      diplome: this.diplome,
      dateInsertion: new Date(),
      dateDerniereVue: new Date()
    };
    console.log(memoire);
    this.fileService.uploadFile(this.fileToUpload).subscribe(data => {
      this.uploadResponse = data;
      memoire.contenu = data.fileDownloadUri;
      // this.fileService.uploadImage(this.imageToUpload).subscribe(data => {
      //   this.uploadResponse = data;
      //   memoire.image = data.fileDownloadUri;
      //   console.log(this.uploadResponse);
        
      // })
      console.log(this.uploadResponse);
      this.memService.ajouterMemoire(memoire).subscribe(mem => {
        this.addSingle("success", "Memoire ajouté avec succes!");
        this.router['admin/memoires'];
      })
    }, error => {
      this.addSingle("echec", error);
      console.log(error);
    });
    this.getAllMemoire();
  }


  addSingle(type: string, details: string) {
    this.messageService.add({key:'state', severity: type, summary:'Status of operation', detail: details});
  }

  fileBrowseHandler(files: FileList){
    this.fileToUpload = files.item(0);
  }

  fileBrowseHandlers(files: FileList){
    this.imageToUpload = files.item(0);
  }

  getAllEtud(){
    this.etudService.etudiantAll().subscribe(data => {
      this.etudiants = data.content;
    })
  }

  getAllEncad(){
    this.encadService.allEncadreurs().subscribe(data => {
      this.encadreurs = data.content;
    })
  }

  getAllDiplome(){
    this.diplomeService.diplomeAll().subscribe(data => {
      this.diplomes = data.content;
    })
  }

  getAllFiliere(){
    this.filireService.filiereAll().subscribe(data => {
      this.filieres = data.content;
    })
  }

  // getAllMemoire(){
  //   this.memService.AllMemoires().subscribe(data => {
  //     this.memoires = data.content;
  //   })
  // }

  getAllMemoire() {
    this.memService.AllMemoires().subscribe(data => {
      let m : Memoire[];
      m = data.content;
      this.memoires = this.memoireDesc(m);
    })
  }

  memoireDesc(memoire: Memoire[]) {
    let t = memoire.length;
    let m : Memoire[];
    m = [];
    for (let i = memoire.length-1; i >=0 ; i--) {
      m.push(memoire[i]);
    }
    return m;
  }

  memoireURL(url: string): SafeResourceUrl {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }


  chargerModal(id){
    this.memService.findById(id).subscribe(data => {
      this.memoire2 = data.body;
    })
  }

}
