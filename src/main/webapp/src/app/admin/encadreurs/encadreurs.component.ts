import { Component, OnInit } from '@angular/core';
import { EncadreursService } from '../../services/encadreurs.service';
import { from } from 'rxjs';
import { Encadreur } from 'src/app/models/Encadraeur';
@Component({
  selector: 'app-encadreurs',
  templateUrl: './encadreurs.component.html',
  styleUrls: ['./encadreurs.component.scss']
})
export class EncadreursComponent implements OnInit {

  encadreurs: Encadreur[] = [];
  encadreur: Encadreur = {
    id: null,
    nomEncadreur: "",
    titreEncadreur: "",
    gradeEncadreur: "",
  };

  constructor(private encadreurservice: EncadreursService) {

    this.encadreurservice.allEncadreurs().subscribe(a => {
      a.content.forEach(element => {
        this.encadreurs.push(element);
      });
    });
  }

  ngOnInit(): void { }

  ajouterEncadreur() {
    this.encadreurservice.addEncadreur(this.encadreur).subscribe(a => {
      this.encadreurs.push(a);
    });
}

supprimerEncadreur(encadreur: Encadreur) {
  this.encadreurservice.deleteEncadreur(encadreur.id).subscribe(a => {
    let index = this.encadreurs.indexOf(encadreur);
    if (index >= 0) {
      this.encadreurs.splice(index, 1);
    }
  }, error => console.log(error));
}
 update(){
   this.encadreurservice.updateEncadreur(this.encadreur).subscribe(a=>{

   });
 }



}
