import { Component, OnInit } from '@angular/core';
import { MemoiresService } from 'src/app/services/memoires.service';
import { Memoire } from 'src/app/models/Memoire';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss']
})
export class AcceuilComponent implements OnInit {
  memoire: Memoire;

  constructor( private memoireService: MemoiresService) { }

  ngOnInit(): void {
    this.memoire = new Memoire();
  }
  
  ajouterMemoire() {
    console.log();  
    this.memoireService.ajouterMemoire(this.memoire).subscribe(
      () => {
       
    });
  }

 
}
