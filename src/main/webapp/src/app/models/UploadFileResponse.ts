
export interface UploadFileResponse {
    fileName: string,
    fileDownloadUri: string,
    status: string,
}
