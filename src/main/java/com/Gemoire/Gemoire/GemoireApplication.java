package com.Gemoire.Gemoire;

import com.Gemoire.Gemoire.config.FileStoragePropertiesConfig;
import com.Gemoire.Gemoire.dao.FiliereDao;
import com.Gemoire.Gemoire.dao.MemoireDao;
import com.Gemoire.Gemoire.entity.Memoire;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
    FileStoragePropertiesConfig.class
})
public class GemoireApplication implements CommandLineRunner{

    @Autowired
    private MemoireDao dao;
    @Autowired
    private FiliereDao filiereDao;
	public static void main(String[] args) {
		SpringApplication.run(GemoireApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
//        String intitule = "informatique de gestion";
//        Long idFiliere = filiereDao.getIdFileire(intitule);
//        System.out.println(idFiliere);
//        
//        List<Memoire> list = dao.getAllMemoireByFiliere(intitule);
//        
//        System.out.println(list);
    }

}
