import { Filiere } from './Filiere';
import{Diplome} from './Diplome';
import{Etudiant} from './Etudiant';
import { Encadreur } from './Encadraeur';

export class Memoire {
    public id: number;
    public titre: String;
    public contenu: String;
    public resume:string;
    public abstract_memoire: String;
    // public image:String;
    public dateInsertion: Date;
    public dateDerniereVue: Date;
    public session: String;
    public filiere: Filiere;
    public diplome: Diplome;
    public etudiant: Etudiant;
    public encadreur: Encadreur;
}