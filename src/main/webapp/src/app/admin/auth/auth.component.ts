import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  username: string;
  user: string;
  password: string;
  pass: string;
  erreur: string;
  er: boolean;

  constructor() { }

  ngOnInit(): void {
    this.user = 'jahaelle';   // ici tu mets ton user
    this.pass = '123456789';  // et ici ton pass
    this.erreur = '';
    this.er = true;
  }

  login() {
    if (this.username != this.user || this.password != this.pass) {
      this.erreur = 'vos identifiants sont incorrects!';
      this.er = false;
      this.username = '';
      this.password = '';
    } else {
      this.er = true;
    }
  }

  link(){
    if (this.username != this.user || this.password != this.pass) {
      return '/admin/auth';
    } else {
      return '/admin/memoires'; 
    }
  }

}
