import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Etudiant } from '../models/Etudiant';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EtudiantService {

  url: string;
  constructor(private http: HttpClient) { 
    this.url = environment.url;
  }

  etudiantAll(): Observable<any> {
    return this.http.get<any>(this.url + `etudiants`);
  }
  addEtudiant(etudiants: Etudiant):Observable<Etudiant>{
    return this.http.post<any>(this.url +'etudiants', etudiants).pipe(map(e=>e.body));
  }
}
