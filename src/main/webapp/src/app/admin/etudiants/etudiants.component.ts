import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EtudiantService } from 'src/app/services/etudiant.service';
import { Etudiant } from 'src/app/models/Etudiant';

@Component({
  selector: 'app-etudiants',
  templateUrl: './etudiants.component.html',
  styleUrls: ['./etudiants.component.scss']
})
export class EtudiantsComponent implements OnInit {
form: FormGroup;
etudiants: Etudiant[] = [];
etudiant: Etudiant={
  id:null,
  nomEtudiant: "",
  matriculeEtudiant: "",
};
  constructor(private etud: EtudiantService) { 

    this.etud.etudiantAll().subscribe(a => {
      a.content.forEach(element => {
        this.etudiants.push(element);
      })
    });
  }

  ngOnInit(): void {

    this.form = new FormGroup({
       nom: new FormControl('', [Validators.required]),
      matricule: new FormControl('', [Validators.required]),
    });
  }

  ajouterEtudiant(){
    this.etud.addEtudiant(this.etudiant).subscribe(a=>{
      this.etudiants.push(a);
    })
  }

}
