import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { AuthComponent } from './auth/auth.component';
import { MemoiresComponent } from './memoires/memoires.component';
import { DepartementsComponent } from './departements/departements.component';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { CardModule } from 'primeng/card';
import { SidebarModule } from 'primeng/sidebar';
import { FieldsetModule } from 'primeng/fieldset';
import { PanelModule } from 'primeng/panel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EncadreursComponent } from './encadreurs/encadreurs.component';
import { FilieresComponent } from './filieres/filieres.component';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { KeyFilterModule } from 'primeng/keyfilter';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { EtudiantsComponent } from './etudiants/etudiants.component';
import { DiplomesComponent } from './diplomes/diplomes.component';
import { FiltersPipePipe } from '../filters-pipe.pipe';

@NgModule({
  declarations: [AcceuilComponent, AuthComponent, MemoiresComponent,
    DepartementsComponent, EncadreursComponent, FilieresComponent, EtudiantsComponent,
    DiplomesComponent, FiltersPipePipe,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    CardModule,
    SidebarModule,
    ButtonModule,
    DropdownModule,
    FieldsetModule,
    PanelModule,
    MessagesModule,
    MessageModule,
    KeyFilterModule,
    ReactiveFormsModule,
    PanelMenuModule,
    ToastModule,
  ],
  providers: [
    MessageService
  ]
})
export class AdminModule { }
