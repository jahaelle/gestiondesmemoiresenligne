import { Component, OnInit, destroyPlatform } from '@angular/core';
import { DepartementsService } from '../../services/departements.service';
import { from } from 'rxjs';
import { Filiere } from 'src/app/models/Filiere';
import { ActivatedRoute } from '@angular/router';
import { Departement } from '../../models/Departement';
//import { Departement } from './Departement';
import { DepartementsComponent } from '../departements/departements.component';
import { FiliereService } from 'src/app/services/filiere.service';
import { Diplome } from 'src/app/models/Diplome';
import { DiplomeService } from 'src/app/services/diplome.service';

@Component({
  selector: 'app-filieres',
  templateUrl: './filieres.component.html',
  styleUrls: ['./filieres.component.scss']
})
export class FilieresComponent implements OnInit {
  filieres: Filiere[];
  diplome: Diplome = {
    id: null,
    codeDiplome: "",
    intituleDiplome: ""
  };
  diplomes: Diplome[];
  idDep: number;
  filiere: Filiere = {
    id: null,
    codeFiliere: "",
    intituleFiliere: "",
    departement: null,
    diplome: null
  };
  constructor(private _service: DepartementsService, private active: ActivatedRoute,
    private filiereService: FiliereService,
    private diplomeService: DiplomeService) {
    active.params.subscribe(params => {
      this.idDep = parseInt(params['id']);
    });
  }

  ngOnInit(): void {
    this.filieresDepartement();
    this.getAllDiplome();
  }

  addFiliereDepartement() {
    this._service.findOneDepartment(this.idDep).subscribe(dep => {
      this.filiere.departement = dep;
      console.log(this.diplome)
      this.filiere.diplome = this.diplome;
      this.filiereService.addFiliere(this.filiere).subscribe(a => {
        this.filieres.push(a);
      });
    });
  }

  filieresDepartement() {
    this._service.findFilieresDepart(this.idDep).subscribe(a => {
      this.filieres = a;
    });
  }

  getAllDiplome(){
    this.diplomeService.diplomeAll().subscribe(data => {
      this.diplomes = data.content;
    })
  }

  supprimerFiliere(filiere: Filiere) {
    this.filiereService.deleteFiliere(filiere.id).subscribe(a => {
      let index = this.filieres.indexOf(filiere);
      if (index >= 0) {
        this.filieres.splice(index, 1);
      }
    }, error => console.log(error));
  }

}
