import { Component, OnInit } from '@angular/core';
import { DepartementsService } from '../../services/departements.service';
import { Departement } from '../../models/Departement';
import {DepartementInput } from '../../models/DepartementInput';
import { FiliereService } from '../../services/filiere.service';
import { Filiere } from '../../models/Filiere';
import { Router, ActivatedRoute } from '@angular/router';
import {MessageService} from 'primeng/api';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-departements',
  templateUrl: './departements.component.html',
  styleUrls: ['./departements.component.scss']
})

export class DepartementsComponent implements OnInit {

  departementForm: FormGroup;
  submitted = false;
  departementId: number;
  codeDepartement: FormControl;
  intituleDepartement: FormControl;
  error = '';
   departementInput: DepartementInput;

  departements: Departement[] = [];
  dep: Departement;
  departement: Departement = {
    id: null,
    codeDepartement: "",
    intituleDepartement: "",
   filieres: null
  };


  constructor(private _service: DepartementsService, private filiereService: FiliereService,
    private router: Router,private messageService: MessageService,) {

    this._service.listAll().subscribe(a => {
      a.content.forEach(element => {
        this.departements.push(element);
      })
    });
  }

  ngOnInit(): void {
    // this.departementId = parseInt(this.router.snapshot.paramMap.get('id'));
    // console.log(this.departementId);
    this.codeDepartement= new FormControl('', Validators.compose([Validators.required, Validators.minLength(4)])),
    this.departementForm = new FormGroup({
      codeDepartement: this.codeDepartement,
      intituleDepartement:this.intituleDepartement,
      
    });

    this._service.findById(this.departementId)
    .subscribe(
      data =>{
        console.log(data);
        this.codeDepartement.setValue(data.body.codeDepartement);
        this.intituleDepartement.setValue(data.body.intituleDepartement);
      },
      error =>{
        this.error = error;
        console.log(error);
        
      }
    )
  }
 


  ajouterDepartement() {
    this._service.addDepartement(this.departement).subscribe(a => {
      this.departements.push(a);
      this.addSingle("success", "departement ajoute avec succes!");
    });
  }

  supprimerDepartement(departement: Departement) {
    this._service.deleteDepartement(departement.id).subscribe(a => {
      let index = this.departements.indexOf(departement);
      if (index >= 0) {
        this.departements.splice(index, 1);
      }
    }, error => console.log(error));
  }

  Update() {
    this.submitted=true;
    if(this.departementForm.invalid){
      return;
    }
    this.departementInput=new DepartementInput(
      this.departementId,
      this.intituleDepartement.value,
      this.codeDepartement.value,
    )
    this._service.updateDepartement(this.departementInput).subscribe(data =>
      console.log(data));
      
  }

  addSingle(type: string, details: string) {
    this.messageService.add({key:'state', severity: type, summary:'Status of operation', detail: details});
  }

}

// get depart(){
//   return this.departementForm.controls;
// }

// update(){
//   this.submitted = true;
//   if(this.auteurForm.invalid){
//     return;
//   }

//   this.auteurInput = new AuteurInput(
//                       this.auteurId,
//                       this.nom.value,
//                     )

//   console.log(this.auteurInput);
//   this.auteurService.update(this.auteurInput)
//   .subscribe(
//     data =>{
//       console.log(data);
//       this.route.navigate(['auteurs/liste-auteur']);
//     },
//     error =>{
//       this.error = error;
//       console.log(error);
//     }
//   )
// }