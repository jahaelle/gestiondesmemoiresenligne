import { Component, OnInit } from '@angular/core';
import { UploadFileResponse } from './../models/UploadFileResponse';
import { MemoiresService } from '../services/memoires.service';
import { Memoire } from '../models/Memoire';
import { FiliereService } from '../services/filiere.service';
import { DiplomeService } from '../services/diplome.service';
import { DepartementsService } from '../services/departements.service';
import { MenuItem } from 'primeng/api';
import { Diplome } from '../models/Diplome';
import { Filiere } from '../models/Filiere';
import { Departement } from '../models/Departement';


@Component({
  selector: 'app-memoire',
  templateUrl: './memoire.component.html',
  styleUrls: ['./memoire.component.scss']
})

export class MemoireComponent implements OnInit {
  display: true;
  fileToUpload: File = null;
  uploadResponse: UploadFileResponse;
  memoires: Memoire[];
  memoire2: Memoire = new Memoire();
  searchText;

// debut ajout

items: MenuItem[] = [];
  //memoires: Memoire[];
  departs: Departement[];
  diplomes: Diplome[];
  filieres: Filiere[];

  item: MenuItem = {
    label: "",
    icon: "",
    items: []
  };


  tmp: MenuItem = {
    label: "",
    icon: "",
    items: []
  };
//fin ajout





  constructor(private memService: MemoiresService,
    private depService: DepartementsService,
    private diplomeService: DiplomeService,
    private filireService: FiliereService) { }

  ngOnInit(): void {
    this.getAllMemoire();
    this.getAllDiplome();
    
  }

  getAllMemoire() {
    this.memService.AllMemoires().subscribe(data => {
      let m : Memoire[];
      m = data.content;
      this.memoires = this.memoireDesc(m);
    })
  }

  memoireDesc(memoire: Memoire[]) {
    let t = memoire.length;
    let m : Memoire[];
    m = [];
    for (let i = memoire.length-1; i >=0 ; i--) {
      m.push(memoire[i]);
    }
    return m;
  }
// debut autre ajout

doItems() {
  let departItem: MenuItem[] = [];
  let filItems: MenuItem[] = [];
  let that = this;
  this.diplomes.forEach(function (dip) {
    that.departs.forEach(function (dep) {
      that.item = {
        label: "",
        icon: "",
        items: []
      };
      that.item.label = dep.intituleDepartement;
      let m = 0;
      that.filieres.forEach(function (fil) {
        that.tmp = {
          label: "",
          icon: "",
        };
        if (dep.codeDepartement == fil.departement.codeDepartement && 
          fil.diplome.codeDiplome == dip.codeDiplome) {
          that.tmp.label = fil.intituleFiliere;

       //   this.items = [ { label: this.diplomes[i].intituleDiplome, command: (event) => { this.test(event.item.label); } }]
      
          filItems[m] = that.tmp;
          console.log(filItems)
          m++;
        }
      });
      //console.lo+g(filItems)
      that.item.items = filItems;
      departItem.push(that.item);
      filItems = [];
      console.log(departItem)
    });
    
    that.item = {
      label: "",
      icon: "",
      items: []
    };
    that.item.label = dip.intituleDiplome;
    that.item.items = departItem;
    that.items.push(that.item);
    departItem = [];
  });
}
/*
getAllMemoire() {
  this.memService.AllMemoires().subscribe(data => {
    this.memoires = data.content;
  })
}
*/

getAllDepartments() {
  this.depService.listAll().subscribe(data => {
    this.departs = data.content;
  })
}

getAllDiplome() {
  this.diplomeService.diplomeAll().subscribe(dip => {
    this.diplomes = dip.content;
    this.depService.listAll().subscribe(dep => {
      this.departs = dep.content;
      this.filireService.filiereAll().subscribe(fil => {
        this.filieres = fil.content;
        this.doItems();
      });
    });
  });
}

getAllFiliere() {
  this.filireService.filiereAll().subscribe(data => {
    this.filieres = data.content;
  })
}

test(value){
  console.log(value);
  
}


chargerModal(id){
  this.memService.findById(id).subscribe(data => {
    this.memoire2 = data.body;
  })
}

}
