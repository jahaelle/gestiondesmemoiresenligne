/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Gemoire.Gemoire.services.impl;

import com.Gemoire.Gemoire.dao.DepartementDao;
import com.Gemoire.Gemoire.dao.FiliereDao;
import com.Gemoire.Gemoire.entity.Departement;
import com.Gemoire.Gemoire.entity.Filiere;
import com.Gemoire.Gemoire.services.IDepartementResource;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author jahaelle
 */
public class DepartementResource implements IDepartementResource{
@Autowired
private DepartementDao departementDao;
@Autowired
private FiliereDao filiereDao;
    @Override
    public ResponseEntity<Departement> addDepartement(Departement departement) {
        Departement de = departementDao.save(departement);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(de.getId())
                .toUri();
        return ResponseEntity.created(location).body(de);
    }

    @Override
    public ResponseEntity<Departement> updateDepartement(long id, Departement departement) {
 return departementDao.findById(id).map(
                de -> {
                    de.setCodeDepartement(departement.getCodeDepartement());
                   de.setIntituleDepartement(departement.getIntituleDepartement());
                    return ResponseEntity.ok(departementDao.save(de));
                }
        ).orElse(
                ResponseEntity.notFound().build()
        );    }
    

    @Override
    public void deleteDepartement(long id) {
        departementDao.deleteById(id);
    }

    @Override
    public Page<Departement> findAllDepartements(int page, int size) {
           return departementDao.findAll(PageRequest.of(page, size));
    }

    @Override
    public Departement findOneDepartement(long id) {
           return departementDao.findById(id).get();
    }

    @Override
    public ResponseEntity<Filiere> ajouterFiliereDepartement(long idDepartement, Filiere filiere) {
      return departementDao.findById(idDepartement).map(
            c ->  {
            filiere.setDepartement(c);
            filiereDao.save(filiere);
            URI location =  ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(filiere.getId())
                    .toUri();
            return ResponseEntity.created(location).body(filiere);
              
              }
      
      ).orElse(ResponseEntity.notFound().build());
    
    
    }

    @Override
    public List<Filiere> findFiliereByDepartement(long idDepartement) {
       return filiereDao.findByDepartement(departementDao.findById(idDepartement).get());
    }

    /*@Override
    public ResponseEntity<Filiere> findFiliereDepartement(long idDepartement, long idFiliere) {
       return filiereDao.findByIdAndDepartementId(idDepartement,idFiliere).map(
               c ->{
        return   ResponseEntity.ok(c);
        
    }) .orElse(ResponseEntity.notFound().build());
    }*/

    }

    
 