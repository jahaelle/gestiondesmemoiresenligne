package com.Gemoire.Gemoire.services;

import com.Gemoire.Gemoire.entity.Diplome;
import com.Gemoire.Gemoire.entity.Etudiant;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.http.ResponseEntity;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import org.springframework.data.domain.Page;

/**
 *
 * @author jahaelle
 */
@Path("/diplomes")
public interface IDiplomeResource {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity<Diplome> addDiplome(Diplome diplome);
    
    @PUT
    @Path("{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<Diplome> updateDiplome(@PathParam("id") long id, Diplome diplome);
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Page<Diplome> findAllDiplomes(@QueryParam("page") @DefaultValue("0") int page, @QueryParam("size") @DefaultValue("10")int size);

    @DELETE
    @Path("{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteDiplome(@PathParam("id") long id);
     
}
