import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Diplome } from '../models/Diplome';

@Injectable({
  providedIn: 'root'
})
export class DiplomeService {

  url: string;
  constructor(private http: HttpClient) { 
    this.url = environment.url;
  }

  diplomeAll(): Observable<any> {
    return this.http.get<any>(this.url + 'diplomes');
  }
  
  addDiplome(diplome: Diplome): Observable<Diplome>{
    return this.http.post<any>(this.url + 'diplomes', diplome);

  }
}
