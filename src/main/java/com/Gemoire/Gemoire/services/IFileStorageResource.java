package com.Gemoire.Gemoire.services;

import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.stereotype.Component;

/**
 *
 * @author jahaelle
 */
@Component
@Path("/file")
public interface IFileStorageResource {

    @POST
    @Path("/uploadFile")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response uploadPdfFile(@FormDataParam("file") InputStream fileInputStream,
            @FormDataParam("file") FormDataContentDisposition fileMetaData);

    @GET
    @Path("/viewFile/{fileName:.+}")
    public Response downloadFile(@PathParam("fileName") String fileName, @Context HttpServletRequest request);
}
