package com.Gemoire.Gemoire.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;

/**
 *
 * @author jahaelle
 */
@Data
@Entity
public class Diplome { 
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

@Column(nullable = false, unique=true)
private String codeDiplome;

@Column(nullable = false, unique=true)
private String intituleDiplome;

@JsonIgnore
@XmlTransient
@OneToMany(mappedBy = "diplome")
private List<Filiere> filiere;

//@OneToMany(mappedBy="diplome")
//private List<Memoire> memoires;

//@JsonIgnore
//@XmlTransient
//@ManyToMany(mappedBy="diplome")
//private List<Filiere> filieres;
      
     

    
}
