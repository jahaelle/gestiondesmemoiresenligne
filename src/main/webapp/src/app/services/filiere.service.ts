import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { Departement } from '../models/Departement';
import { Filiere } from '../models/Filiere';

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  url: string;
  constructor(private http: HttpClient) { 
    this.url = environment.url;
  }

  filiereAll(): Observable<any> {
    return this.http.get<any>(this.url + `filieres`);
  }
  
  addFiliere(filiere: Filiere): Observable<Filiere> {
    return this.http.post<any>(this.url + 'filieres', filiere).pipe(map(e => e.body));
  }
  deleteFiliere(id: number): Observable<any> {
    return this.http.delete<any>(this.url + 'filieres/' + id);
  }

}
