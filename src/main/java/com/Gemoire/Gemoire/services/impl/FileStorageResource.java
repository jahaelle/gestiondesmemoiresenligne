package com.Gemoire.Gemoire.services.impl;

import com.Gemoire.Gemoire.entity.UploadFileResponse;
import com.Gemoire.Gemoire.exception.DataAccessException;
import com.Gemoire.Gemoire.exception.FileStorageException;
import com.Gemoire.Gemoire.exception.MyFileNotFoundException;
import com.Gemoire.Gemoire.services.FileStorageService;
import com.Gemoire.Gemoire.services.IFileStorageResource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Sprint-Pay
 */
public class FileStorageResource implements IFileStorageResource {
    
    @Autowired
    private FileStorageService fileStorageService;
    
    @Override
    public Response uploadPdfFile(InputStream fileInputStream, FormDataContentDisposition fileMetaData) {
        try {
            String fileName;
            try {
                fileName = fileStorageService.storeFile(fileInputStream, fileMetaData);
            } catch (FileStorageException ex) {
                System.out.println(ex.getMessage());
                return Response.status(Response.Status.OK)
                        .entity("{\"error\": \" Une erreur inattendue est survenue... \"}")
                        .build();
            }

            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/api/file/viewFile/")
                    .path(fileName)
                    .toUriString();

            return Response.status(Response.Status.OK)
                    .entity(new UploadFileResponse(fileName, fileDownloadUri, "success"))
                    .build();
        } catch (DataAccessException ex) {
            Logger.getLogger(FileStorageResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.OK)
                    .entity("{\"error\": \" Une erreur inattendue est survenue...\"}")
                    .build();
        }
    }

    @Override
    public Response downloadFile(String fileName, HttpServletRequest request) {
        try {
            Resource resource;
            // Load file as Resource
            try {
                resource = fileStorageService.loadFileAsResource(fileName);
            } catch (MyFileNotFoundException ex) {
                return Response.status(Response.Status.OK)
                        .entity("{\"error\": \"" + ex + "\"}")
                        .build();
            }

            // Try to determine file's content type
            String contentType;
            try {
                contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            } catch (IOException ex) {
                contentType = "application/octet-stream";
            }
            // Fallback to the default content type if type could not be determined
            if (contentType == null) {
                contentType = "application/octet-stream";
            }
            File file;
            try {
                file = resource.getFile();
            } catch (IOException ex) {
                Logger.getLogger(FileStorageResource.class.getName()).log(Level.SEVERE, null, ex);
                return Response.status(Response.Status.OK)
                        .entity("{\"error\": \"An error occurred while getting the file!\"}")
                        .build();
            }
            Response.ResponseBuilder rb = Response.ok(file, contentType);
            rb.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"");
            return rb.build();
        } catch (DataAccessException ex) {
            Logger.getLogger(FileStorageResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.OK)
                    .entity("{\"error\": \" Une erreur inattendue est survenue...\"}")
                    .build();
        }
    }
    
}
