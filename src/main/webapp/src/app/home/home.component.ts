  import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Departement } from '../models/Departement';
import { Diplome } from '../models/Diplome';
import { Filiere } from '../models/Filiere';
import { MemoiresService } from '../services/memoires.service';
import { DepartementsService } from '../services/departements.service';
import { DiplomeService } from '../services/diplome.service';
import { FiliereService } from '../services/filiere.service';
import { Memoire } from '../models/Memoire';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  items: MenuItem[] = [];
  memoires: Memoire[];
  departs: Departement[];
  diplomes: Diplome[];
  filieres: Filiere[];

  item: MenuItem = {
    label: "",
    icon: "",
    items: []
  };
  
  tmp: MenuItem = {
    label: "",
    icon: "",
    items: []
  };

  constructor(private memService: MemoiresService,
    private depService: DepartementsService,
    private diplomeService: DiplomeService,
    private filireService: FiliereService) {
    
  }

  ngOnInit(): void {
    this.getAllDiplome();
  }

  doItems() {
    let departItem: MenuItem[] = [];
    let filItems: MenuItem[] = [];
    let that = this;
    this.diplomes.forEach(function (dip) {
      that.departs.forEach(function (dep) {
        that.item = {
          label: "",
          icon: "",
          items: []
        };
        that.item.label = dep.intituleDepartement;
        let m = 0;
        that.filieres.forEach(function (fil) {
          that.tmp = {
            label: "",
            icon: "",
          };
          if (dep.codeDepartement == fil.departement.codeDepartement && 
            fil.diplome.codeDiplome == dip.codeDiplome) {
            that.tmp.label = fil.intituleFiliere;
            filItems[m] = that.tmp;
            console.log(filItems)
            m++;
          }
        });
        //console.lo+g(filItems)
        that.item.items = filItems;
        departItem.push(that.item);
        filItems = [];
        console.log(departItem)
      });
      
      that.item = {
        label: "",
        icon: "",
        items: []
      };
      that.item.label = dip.intituleDiplome;
      that.item.items = departItem;
      that.items.push(that.item);
      departItem = [];
    });
  }

  getAllMemoire() {
    this.memService.AllMemoires().subscribe(data => {
      this.memoires = data.content;
    })
  }

  getAllDepartments() {
    this.depService.listAll().subscribe(data => {
      this.departs = data.content;
    })
  }

  getAllDiplome() {
    this.diplomeService.diplomeAll().subscribe(dip => {
      this.diplomes = dip.content;
      this.depService.listAll().subscribe(dep => {
        this.departs = dep.content;
        this.filireService.filiereAll().subscribe(fil => {
          this.filieres = fil.content;
          this.doItems();
        });
      });
    });
  }

  getAllFiliere() {
    this.filireService.filiereAll().subscribe(data => {
      this.filieres = data.content;
    })
  }

  test(){
    console.log("ok");
    
  }

}
