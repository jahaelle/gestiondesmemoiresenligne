import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { MemoiresComponent } from './memoires/memoires.component';
import { DepartementsComponent } from './departements/departements.component';
import { EncadreursComponent } from './encadreurs/encadreurs.component';
import { FilieresComponent } from './filieres/filieres.component';
import { EtudiantsComponent } from './etudiants/etudiants.component';
import { DiplomesComponent } from './diplomes/diplomes.component';

const routes: Routes = [
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  { path: 'auth', component: AuthComponent},
  {path: 'acceuil', component: AcceuilComponent},
   {path: 'memoires', component: MemoiresComponent},
   {path: 'departements', component: DepartementsComponent},
   {path: 'encadreurs', component: EncadreursComponent},
   {path: 'filieres', component: FilieresComponent},
   {path: 'filieres/:id', component: FilieresComponent},
   {path: 'etudiants', component: EtudiantsComponent},
   {path: 'diplomes', component: DiplomesComponent}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
